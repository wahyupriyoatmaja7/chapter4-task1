let students = [
  {
    name: "Maria",
    provinsi: "Jombang",
    age: 21,
    Single: true,
  },
  {
    name: "Gusde",
    provinsi: "Bali",
    age: 27,
    Single: false,
  },
  {
    name: "Wahyu",
    provinsi: "Jawa Tengah",
    age: 20,
    Single: false,
  },
  {
    name: "Chandra",
    provinsi: "Papua",
    age: 23,
    Single: false,
  },
  {
    name: "Imam",
    provinsi: "Batam",
    age: 27,
    Single: true,
  },
];

function locationn(loc) {
  return loc == "Jawa Tengah" ? loc : "bukan Jawa Tengah";
}

function Age(age) {
  return age < 21 ? "di bawah 22" : "lebih dari sama dengan 22";
}

function Single(status) {
  return status == true ? "single" : "sudah punya pacar";
}

function printOutput(cb1, cb2, cb3) {
  for (const key in students) {
    let myLocation = cb1(students[key].provinsi);
    let myAge = cb2(students[key].age);
    let myStatus = cb3(students[key].Single);
    if (
      myLocation == "Jawa Tengah" &&
      myAge == "di bawah 22" &&
      myStatus == "sudah punya pacar"
    ) 
    {
      console.log(
        `Nama saya ${students[key].name}, saya tinggal di ${myLocation}, umur saya ${myAge}, dan saya ${myStatus} loh. CODE buat nilam`
      );
    }
  }
}

printOutput(locationn,Age, Single);
